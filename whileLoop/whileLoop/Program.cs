﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace whileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 10;
            var i = 0;

            while (i < count)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
                i++;
            }
        }
    }
}
